<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClickControllerTest extends WebTestCase
{
    public function testApiClicksCount()
    {
        $client = static::createClient();

        $client->request('GET', '/clicks');

        $jsonResponse = json_decode($client->getResponse()->getContent());

        $this->assertObjectHasAttribute("clicks", $jsonResponse);
    }

    public function testApiClickInsert()
    {
        $client = static::createClient();

        $client->request('GET', '/click');

        $jsonResponse = json_decode($client->getResponse()->getContent());

        $this->assertObjectHasAttribute("status", $jsonResponse);

        $this->assertEquals("success", $jsonResponse->status);

    }
}